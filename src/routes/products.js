const { Router } = require('express');
const router = Router();

const MySqlConnection = require('../database');

///////////
///de la parte de la tabla producto
///////////

router.get('/api/products/all', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    mysqlConnection.connection.query('select * from tbl_producto', (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.get('/api/products/:producto_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { producto_id } = req.params;
    mysqlConnection.connection.query('select * from tbl_producto where producto_id = ?', [producto_id], (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});


router.get('/api/products/filtro/:usuario_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { usuario_id } = req.params;
    mysqlConnection.connection.query('select * from tbl_producto where usuario_id = ?', [usuario_id], (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.get('/api/products/filtroconteo/:usuario_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { usuario_id } = req.params;
    mysqlConnection.connection.query('select AVG(producto_calorias) as CALORIAS, AVG(producto_proteinas) AS PROTEINAS, AVG(producto_grasas) AS GRASAS from tbl_producto where usuario_id = ?', [usuario_id], (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});


router.post('/api/products/register', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { producto_id, producto_nombre, producto_calorias, producto_proteinas, producto_grasas, producto_carbohidratos, usuario_id } = req.body;
    console.log("register products post ")

    console.log(req.body);
    mysqlConnection.connection.query('insert into tbl_producto(producto_nombre, producto_calorias, producto_proteinas,producto_grasas, producto_carbohidratos, usuario_id ) values (?, ?, ?, ?, ?, ?)', [producto_nombre, producto_calorias, producto_proteinas, producto_grasas, producto_carbohidratos, usuario_id], (error, rows, fields) => {
        if (!error) {
            res.json(rows);
        } else {
            console.log(error);
        }
        mysqlConnection.disconnect();
    });
});

router.put('/api/products/:producto_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { producto_carbohidratos, producto_nombre, producto_calorias, producto_proteinas, producto_grasas, usuario_id } = req.body;
    const { producto_id } = req.params;

    console.log("put rp")

    console.log(req.body);

    mysqlConnection.connection.query('update tbl_producto set producto_nombre = ?, producto_calorias = ?, producto_proteinas = ?, producto_grasas = ?, producto_carbohidratos=? ,usuario_id = ? where producto_id = ?;',
        [producto_nombre, producto_calorias, producto_proteinas, producto_grasas, producto_carbohidratos, usuario_id, producto_id], (error, rows, fields) => {
            if (!error) {
                res.json({
                    Status: 'Producto actualizado'
                });
            } else {
                console.log(error);
            }
            mysqlConnection.disconnect();
        });
});
router.delete('/api/products/:producto_id', (req, res) => {
    const mysqlConnection = new MySqlConnection();
    mysqlConnection.connect();
    const { producto_id } = req.params;
    mysqlConnection.connection.query('delete from tbl_producto where producto_id = ?', [producto_id], (error, rows, fields) => {
        if (!error) {
            res.json({
                Status: "Producto eliminado"
            });
        } else {
            res.json({
                Status: error
            });
        }
        mysqlConnection.disconnect();
    });
});

module.exports = router;
